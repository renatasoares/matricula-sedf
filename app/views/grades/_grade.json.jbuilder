json.extract! grade, :id, :space_available, :name, :created_at, :updated_at
json.url grade_url(grade, format: :json)
