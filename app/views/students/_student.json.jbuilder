json.extract! student, :id, :latitude, :longitude, :name, :cep, :birth_data, :responsible_name, :responsible_cpf, :intented_grade, :created_at, :updated_at
json.url student_url(student, format: :json)
