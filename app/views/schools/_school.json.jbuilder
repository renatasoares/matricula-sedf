json.extract! school, :id, :name, :city_name, :room_quantity, :created_at, :updated_at
json.url school_url(school, format: :json)
