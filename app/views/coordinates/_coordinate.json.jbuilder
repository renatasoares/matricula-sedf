json.extract! coordinate, :id, :latitude, :longitude, :created_at, :updated_at
json.url coordinate_url(coordinate, format: :json)
