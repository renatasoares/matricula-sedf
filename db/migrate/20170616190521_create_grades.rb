class CreateGrades < ActiveRecord::Migration
  def change
    create_table :grades do |t|
      t.integer :space_available
      t.string :name
      
      t.belongs_to :school
      t.timestamps null: false
    end
  end
end
