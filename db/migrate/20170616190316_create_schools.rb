class CreateSchools < ActiveRecord::Migration
  def change
    create_table :schools do |t|
      t.string :name
      t.string :city_name
      t.integer :room_quantity

      t.belongs_to :coordinate
      t.belongs_to :register
      t.timestamps null: false
    end
  end
end

