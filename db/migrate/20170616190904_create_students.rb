class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.float :latitude
      t.float :longitude
      t.string :name
      t.string :cep
      t.date :birth_data
      t.string :responsible_name
      t.string :responsible_cpf
      t.string :intented_grade

      t.belongs_to :register
      t.timestamps null: false
    end
  end
end
